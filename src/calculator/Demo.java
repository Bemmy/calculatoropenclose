/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

public class Demo {
    public static void main(String[] args){
        
        Addition additionCalculator = new Addition(2.8,5.9);
        
        System.out.println("Addition result of " + additionCalculator + " = " + additionCalculator.calculate());
        
        Subtraction subtractionCalculator = new Subtraction(98.5, 4.7);
        
        System.out.println("Subtraction result of " + subtractionCalculator + " = " 
                + subtractionCalculator.calculate());
    }
}
