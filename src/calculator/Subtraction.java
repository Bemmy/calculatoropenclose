/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

public class Subtraction extends Calculator {
    
      public Subtraction(double operand1, double operand2){
        super(operand1, operand2);
       
        this.operand1 = operand1;
        this.operand2 = operand2;
    }
    public double calculate(){
        return operand1 - operand2;
    }
    
    public String toString(){
        
        return operand1 + " and " + operand2;
    }
}
